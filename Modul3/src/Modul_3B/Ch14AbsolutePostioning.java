package Modul_3B;
import java.awt.Color;
import java.awt.Container;
import javax.swing.*;

public class Ch14AbsolutePostioning extends JFrame {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 300;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;
    
    public static void main (String[] args) {
        Ch14AbsolutePostioning frame = new Ch14AbsolutePostioning();
        frame.setVisible(true);
    }

    public Ch14AbsolutePostioning() {
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT);
        setResizable ( true );
        setTitle ( "program Ch14AbsolutePostioning");
        setLocation ( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(null);
        contentPane.setBackground( Color.white );
        
        okButton = new JButton ("OK");
        okButton.setBounds(110,110, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton = new JButton ("CANCEL");
        cancelButton.setBounds (200, 110, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        setDefaultCloseOperation ( EXIT_ON_CLOSE );
        
    }
}
   