package Modul_3A;

import javax.swing.JFrame;

public class FrameKu_Latihan1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ini Frame Pertamaku");
        
        int tinggi = 400;
        int lebar = 400;
        
        frame.setBounds(0, 0, lebar, tinggi);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}