/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4_195314148;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JRootPane;
/**
 *
 * @author admin
 */
public class Latian_2 extends JFrame implements ActionListener {
    /**
     * @param args the command line arguments
     */
    private static final int FRAME_WIDTH = 450;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_X_ORIGIN = 850;
    private static final int FRAME_Y_ORIGIN = 200;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JButton button;

    public Latian_2() {
        Container contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());

        //set the frame properties
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Contoh");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        //create and place two buttons on the frame's content pane
        button = new JButton("Click Me");
        button.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button);

        //register 'Exit upon closing' as a default close operation
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        //registering a ButtonHandler as an action Listener of the two buttons
        button.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clickedButton = (JButton) e.getSource();

        JRootPane rootPane = clickedButton.getRootPane();
        JFrame frame = (JFrame) rootPane.getParent();

        String buttonText = clickedButton.getText();
        frame.setTitle("(Dibuat dengan cara 2) You clicked " + buttonText);
    }

    public static void main(String[] args) {
        // TODO code application logic here
        Latian_2 lat2 = new Latian_2();
        lat2.setVisible(true);
    }
}
