/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4_195314148;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;

/**
 *
 * @author admin
 */
public class ButtonHandler implements ActionListener{
    public ButtonHandler(){
        
    }
    
    @Override
    public void actionPerformed(ActionEvent event){
        JButton clickedButton = (JButton) event.getSource();
        
        JRootPane rootPane = clickedButton.getRootPane();
        JFrame frame = (JFrame) rootPane.getParent();
        String buttonText = clickedButton.getText();
        
        frame.setTitle("You clicked " + buttonText);
        
    }
}