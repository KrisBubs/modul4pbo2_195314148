/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4_195314148;

//untuk import class java
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Tugas1 extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;//untuk mengatur lebar
    private static final int FRAME_HEIGHT = 200;//untuk mengatur tinggi
    private static final int FRAME_X_ORIGIN = 950;//untuk mengatur muncul nya Jframe dalam posisi koordinat x
    private static final int FRAME_Y_ORIGIN = 200;//untuk mengatur muncul nya Jframe dalam posisi koordinat y
    private static final int BUTTON_WIDTH = 80;//untuk mengatur lebar tombbol
    private static final int BUTTON_HEIGHT = 30;//untuk mengatur tinggi tombol
    private JButton button;//deklarasi objek button dari class JButton
    private Container content = getContentPane();//deklarsi objek content dari class Container dan 
                                                //memanggil method getContentPane
    private JTextField field1, field2, field3;//deklarasi objek field 1-3 dari class JTextField
    private JLabel label1, label2, label3;//deklarasi objek label 1-3 dari class JLabel
    private int a, b, hasil;//deklarasi objek a,b,hasil bertipe data int

    public Tugas1() {//constructor
        this.setTitle("Contoh");//mengatur judul dari JFrame
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);//mengatur ukuran dengan memanggil variabel yang sudah dideklarasikan
        this.setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);//mengatur koordinat x dan y dengan memanggil variabel 
                                                        //yang sudah dideklarasikan
        this.setLayout(null);//untuk mengatur layout 

        label1 = new JLabel("Bilangan 1");//untk memberi judul dari Label yang di panggil
        label1.setBounds(20, 10, 250, 50);//untuk mengatur letak dari label 1 dalam Jframe
        content.add(label1);//untuk memanggil label 1 

        field1 = new JTextField(10);//untuk memberi judul dari field yang di panggil
        field1.setBounds(120, 25, 100, 20);//untuk mengatur letak dari field 1
        content.add(field1);//untuk memanggil field 1

        label2 = new JLabel("Bilangan 2");//untuk memberi judul dari label yang di panggil
        label2.setBounds(20, 35, 200, 50);//untuk mengatur letak dari label 2
        content.add(label2);//untuk memanggil label 2

        field2 = new JTextField(10);//untuk memberi judul dari field yang di panggil
        field2.setBounds(120, 50, 100, 20);//untuk mengatur letak dari field 2
        content.add(field2);//untuk memanggil field 2

        label3 = new JLabel("Hasil");//untuk memberi judul dari label yang di panggil
        label3.setBounds(20, 60, 200, 50);//untuk mengatur letak dari label 3
        content.add(label3);//untuk memanggil label 3 

        field3 = new JTextField(10);//untuk memberi judul dari field yang di panggil
        field3.setBounds(120, 75, 100, 20);//untuk mengatur letak dari field 3
        content.add(field3);//untuk memanggil field 3

        button = new JButton("Jumlah");//untuk memberi judul dari button yang di 0panggil
        button.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);//untuk mengatur button dengan ukuran yang dideklarasikan
        button.setBounds(120, 100, 100, 20);//untuk mengatur letak dari button
        button.addActionListener(this);//untuk menambahkan action listener
        content.add(button);//untuk memanggil button

        ButtonHandler handler = new ButtonHandler();//membuat objek handler dari Buttonhandler
        button.addActionListener(handler);//untuk menambahkan handler

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);//untuk keluar dari JFrame jika di klik tombol X
    }

    @Override
    public void actionPerformed(ActionEvent e) {//method oberride actonPerformed bertipe ActionEvent
        a = Integer.parseInt(field1.getText());//agar apapun masukan yang diberikan pada a akan di ubah menjadi int
        b = Integer.parseInt(field2.getText());//agar apapun masukan yang diberikan pada b akan di ubah menjadi int
        hasil = a + b;//rumus penjumlahan
        field3.setText(Integer.toString(hasil));//mengkonversi hasil yang bertipe int menjadi String
    }
    
    public static void main(String[] args) {//main
        Tugas1 tugas = new Tugas1();
        tugas.setVisible(true);
    }
}
