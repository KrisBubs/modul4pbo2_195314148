/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4_195314148;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.JTextField;
/**
 *
 * @author Lenovo
 */
public class Latian_4 extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 450;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_X_ORIGIN = 850;
    private static final int FRAME_Y_ORIGIN = 200;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JButton button1, button2;
    private Container content = getContentPane();
    private JTextField field;

    public Latian_4() {
        this.setTitle("Contoh");
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        this.setLayout(new FlowLayout());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        field = new JTextField();
        field.setColumns(20);
        content.add(field);

        //create and place two buttons on the frame's content pane
        button1 = new JButton("Click Me");
        button1.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        button1.addActionListener(this);
        content.add(button1);

        button2 = new JButton("Tombol 2");
        button2.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        button2.addActionListener(this);
        content.add(button2);

        //register a ButtonHeadler as an action listener of the two buttons
        ButtonHandler handler = new ButtonHandler();
        button1.addActionListener(handler);
        button2.addActionListener(handler);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clickedButton = (JButton) e.getSource();

        JRootPane rootPane = clickedButton.getRootPane();
        JFrame frame = (JFrame) rootPane.getParent();

        String buttonText = clickedButton.getText();
        frame.setTitle("(Dibuat dengan cara 2) You clicked " + buttonText);

        if (buttonText.equals("Click Me")) {
            String isi = field.getText();
            frame.setTitle(isi);
        } else {
            frame.setTitle("You Clicked " + buttonText);
        }
    }
    public static void main(String[] args) {
        Latian_4 lat4 = new Latian_4();
        lat4.setVisible(true);
    }
}
